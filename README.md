Ansible Certbot Dockerized
=========

Ansible role to deploy a certbox docker container.

Requirements
------------

Docker

Role Variables
--------------

| Name                               | Value                     |
|------------------------------------|---------------------------|
| ansible_certbot_image              | "certbot/certbot:latest"  |
| ansible_certbot_project_directory  | "/srv/certbot"            |
| ansible_certbot_container_name     | "CertBot"                 |
| ansible_certbot_host_ip            | "0.0.0.0"                 |
| ansible_Certbot_host_port          | 80                        |

License
-------

MIT
